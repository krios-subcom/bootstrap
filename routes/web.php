<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/**
 * FRONTEND
 */
Route::get('/', function () {

    return view('frontend');

});

Route::get('/login', function () {

    return view('frontend');

});

Route::get('/register', function () {

    return view('frontend');

});


Route::group(['middleware' => 'auth'], function() {

    Route::get('/home', function () { return view('backend'); });

});


