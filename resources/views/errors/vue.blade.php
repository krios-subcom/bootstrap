<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>

        <title> @yield('title') - {{ config('app.name') }}</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    
    </head>

    <body>

        <div id="app">

            <v-app>

                <error :code="@yield('code')" message="@yield('message')"></error>

            </v-app>

        </div>

        <script src="{{ asset('js/app.js?key='.config('app.key')) }}" ></script>

    </body>

</html>
