<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

    @include('layouts.frontend.head')

    <body>

        <div id="app">

            <v-app :dark="{{ config('layout.dark') ? 'true' : 'false' }}">

                <app-fe-toolbar app-name="{{ config('app.name', 'Laravel') }}" @if (Auth::check()) :logged="true" @endif ></app-fe-toolbar>

                <v-content>

                    <v-container fluid fill-heigh>

                        <v-layout align-center justify-center>

                            <v-flex xs12 sm8 md4>

                                <v-fade-transition leave-absolute>

                                    <router-view :key="$route.fullPath"></router-view>

                                </v-fade-transition>

                            </v-flex>

                        </v-layout>

                    </v-container>

                </v-content>

                <app-notifications ref="notifications"></app-notifications>

            </v-app>

        </div>

        <script src="{{ asset('js/app.js?'.config('app.key')) }}" ></script>

    </body>

</html>
