<head>

        <title>{{ config('app.name') }}</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

        <script>

            window.App = { errors : {!! $errors !!}, status : '{!! session('status') !!}', warning:  '{!! session('warning') !!}', success : {} };
        
        </script>
        

    </head>
