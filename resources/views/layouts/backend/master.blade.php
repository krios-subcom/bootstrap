<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

    @include('layouts.backend.head')

    <body>

        <div id="app">

            <v-app :dark="{{ config('layout.dark') ? 'true' : 'false' }}">

                <app-be-toolbar ref="topbar"  @toggle-drawer="$refs.sidebar.drawer = !$refs.sidebar.drawer"  app-name="{{ config('app.name', 'Laravel') }}" @if (Auth::check()) :logged="true" @endif ></app-be-toolbar>

                <app-be-sidebar ref="sidebar"></app-be-sidebar>

                <v-content>

                    <v-container fluid >

                        <v-flex offset-sm1 sm10 xs12>

                            <v-fade-transition leave-absolute>

                                <router-view :key="$route.fullPath"></router-view>

                            </v-fade-transition>

                        </v-flex>

                    </v-container>

                </v-content>

                <app-notifications ref="notifications"></app-notifications>

            </v-app>

        </div>

        <script src="{{ asset('js/app.js?'.config('app.key')) }}" ></script>

    </body>

</html>
