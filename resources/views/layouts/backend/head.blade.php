<head>

        <title>{{ config('app.name') }}</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

        <script>

        @if ( ! Auth::check() )
            window.document.location.href = '{!! url('/login') !!}';
        @else
            window.App = { errors : {!! $errors !!}, status : '{!! session('status') !!}', warning:  '{!! session('warning') !!}', success : {} };
            window.App.user = {!! auth()->user() !!};
            window.App.side_bar = {!! json_encode(Config::get('menu.sidebar')); !!};
        @endif
            
        </script>
        

    </head>
