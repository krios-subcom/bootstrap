/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuetify   from 'vuetify';
import VueRouter from 'vue-router';
import VueI18n   from 'vue-i18n';

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueI18n);

import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css'


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

/**
 * COMMON COMPONENTS
 */
Vue.component('app-notifications', require('./components/common/Notifications.vue').default);
Vue.component('csrf-token',        require('./components/common/CsrfToken.vue').default);
Vue.component('error',             require('./components/common/Error.vue').default);

/**
 * FRONTEND
 */
Vue.component('app-fe-toolbar', require('./components/frontend/Toolbar.vue').default);


/**
 * BACKEND
 */
Vue.component('app-be-toolbar', require('./components/backend/Toolbar.vue').default);
Vue.component('app-be-sidebar', require('./components/backend/Sidebar.vue').default);

/**
 *  MIXIN
 */
import axiosWrapper from './mixins/axiosWrapper';
import utils from './mixins/utils';
  
Vue.mixin(axiosWrapper);
Vue.mixin(utils);

/**
 * ROUTES
 */
import { routes } from './routes.js';

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes
});
  
  
/**
 * LOCALIZATION
 */
import { languages } from './i18n/index.js';
import { defaultFallbackLanguage } from './i18n/index.js';

const messages = Object.assign(languages);

let locale = process.env.MIX_APP_LOCALE;

const i18n = new VueI18n({

    locale: locale,

    fallbackLocale: defaultFallbackLanguage,

    messages
});


if( !window.hasOwnProperty('App')){

    window.App = { errors : [], status : '', warning:  '', success : {} } ;
    
}


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({

    el: '#app',

    router,

    i18n,

    data() {

        return {

            status: window.App.status,

            errors: window.App.errors,

            warning: window.App.warning,

            success: window.App.success,

            user: window.App.user,

            dark: false,

            rules : {
                
                required : value => !!value || this.$t('required'),

                email: v => /.+@.+/.test(v) || this.$t('e-mail not valid')

            }
            

        }

    },

    watch: {

        /**
         * 
         */
        status: function()
        {
            this.$refs.notifications.status = this.status;
        },

        /**
         * 
         */
        errors: function()
        {
            this.$refs.notifications.errors = this.errors;
        },

        /**
         * 
         */
        warning: function()
        {
            this.$refs.notifications.warning = this.warning;
        },

        /**
         * 
         */
        success: function()
        {
            this.$refs.notifications.success = this.success;
        },

        /**
         * 
         */
        user: function()
        {      
            
            
        },

    },

    methods: {
      
        /**
         * 
         */
        resetNotifications: function()
        {
            this.errors = [];

            this.status = '';

            this.warning = '';

            this.success = {} ;
        }


    },




});
