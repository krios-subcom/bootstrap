/**
 * 
 * AXIOS WRAPPER
 * 
 */
export default {

    /**
     * 
     */
    data () {

        return {}

    },
    
    methods: {

        /**
         * 
         * @param {string} method 
         * @param {string} url 
         * @param {FormData} data 
         * @param {Function} callback 
         * @param  {...any} args 
         */
        call: function( method, url, data = new FormData(), headers = [], callback, ...args)
        {

            let options = {

                method  : method,
                url     : url,
                data    : data,
                headers : headers

            }

            axios(options)

                .then(function (response) {

                    callback(response, ...args)

                }).catch( e => {
                    
                    this.handleError(e);
                    
                });


        },

        /**
         * 
         * @param {String} url 
         * @param {Function} callback 
         * @param  {...any} args 
         */
        get: function( url, callback, ...args)
        {      
            this.call('get', url, null, null,  callback, ...args);
        },

        /**
         * 
         * @param {String} url 
         * @param {Form} form 
         * @param {Function} callback 
         * @param  {...any} args 
         */
        post: function(url, form = new FormData(), callback, ...args ) {

            this.call('post', url, form, null, callback, ...args);

        },

        /**
         * 
         * @param {String} url 
         * @param {Function} callback 
         * @param  {...any} args 
         */
        delete: function(url, callback, ...args)
        {
            this.call('delete', url, [], null,  callback, ...args);

        },

        /**
         * 
         * @param {String} url 
         * @param {FormData} form 
         * @param {Function} callback 
         * @param  {...any} args 
         */
        patch: function(url, form = new FormData(), callback, ...args ) {

            this.call('patch', url, form, null, callback, ...args);

        },
        
        /**
         * 
         * @param {String} url 
         * @param {Form} form 
         * @param {Function} callback 
         * @param  {...any} args 
         */
        upload : function(url, form = new FormData(), callback, ...args)
        {

            let headers = {

                'Content-Type': 'multipart/form-data'
            };

            this.call('post', url, form, headers, callback, ...args);

        },

        /**
         * 
         * @param {Error} error 
         */
        handleError: function(error)
        {   

            let message = error.response.statusText;

            let status  = error.response.status;

            if(error.response.data.hasOwnProperty('message'))
            {
                message = error.response.data.message;
            }

            this.$root.errors = { 'oops' : [ message ]  };

            this.handleErrorStatusCode(status);
        },

        /**
         * 
         * @param {int} code 
         */
        handleErrorStatusCode: function(code)
        {
            
            switch(code){
                
                case 404:
                case 403:
                    window.document.location.href = process.env.MIX_404_REDIRECT;
                    break;
            }
            
        }
            
    }

}
