/**
 * 
 * Wra
 * 
 */

import moment from 'moment';

export default {

    /**
     * 
     */
    data () {

        return {

        }

    },

    mounted() {

    },
    
    /**
     * 
     */
    methods: {
        
        /**
         * 
         * @param {Array} array 
         * @param {String} separator 
         * @param {String} key 
         */
        explodeArrayList: function(array, separator, key)
        {
            let temp = [];

            array.forEach(element => {
                
                temp.push(element[key]);

            });
            
            return temp.join(separator);
        },

        /**
         * 
         * @param {Object} object 
         */
        isEmpty: function(object)
        {
            for(var key in object) {
                if(object.hasOwnProperty(key))
                    return false;
            }
            return true;
        },

        /**
         * 
         * @param {String} string 
         */
        isValidJSONString: function(string) {
            try {
                JSON.parse(string);
            } catch (e) {
                return false;
            }
            return true;
        },

        /**
         * 
         * @param {*} objects 
         * @param {String} key 
         * @param {String} value 
         */
        objectsContains: function(objects, key, value)
        {   
            let exists = false;

            for(var index = 0; index < objects.length; index ++) {

                if(objects[index].hasOwnProperty(key)) {

                    exists =  objects[index][key] == value;
                    
                }

            }

           return exists;
        },

        /**
         * 
         */
        goBack: function ()
        {
            this.$router.go(-1);
        },

        /**
         * 
         * @param {Integer} time 
         * @param {String} format 
         */
        formatTime: function(time, type = 'minutes', format = 'hh:mm')
        {
            if(parseInt(time) == 30)
            {
                return '00:30';
            }
                
            return moment.duration(parseInt(time), type).format( format ) ;
        }


    }

}
