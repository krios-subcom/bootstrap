/**
 * 
 */
import en from './en.json'

export const defaultFallbackLanguage = 'en'

export const languages = {

  en: en
  
}
