import Homepage from './components/frontend/Homepage.vue';
import Login from './components/frontend/auth/Login.vue';
import Register from './components/frontend/auth/Register.vue';

import Home from './components/backend/Home.vue';


export const routes = [

    {
        path: '',
        component: Homepage
    },

    {
        path: '/login',
        component: Login
    },

    {
        path: '/register',
        component: Register
    },

    {
        path: '/home',
        component: Home
    },

]